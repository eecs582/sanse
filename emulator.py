#--- State-Aware Network/System Emulator ---#
from xml.dom import minidom
from thread import *
import time
import commands
import os
import socket
import sys
from linux import *
from TypeUtils import *

# control channel config
HOST = ''
PORT = 8888

# sysemu config
MAX_TRY=100
MIN_CPU=1
MAX_CPU=100
pid = ''
limit = '100'
pid_group = []
monitor_group = []

# netemu config
MIN_DELAY=0
MAX_DELAY=2000
MIN_BW=0
MAX_BW=10000
MIN_DROP=0
MAX_DROP=100
NET_DEFAULTS='scenes/net.defaults.xml'
NETEMU_LAN = 'wlx00c0ca8448ec'
NETEMU_WAN = 'eno1'

# eval config
SYSFILE = 'sys.log'
NETFILE = 'net.log'
QOEFILE = 'qoe.log'
INTERVAL = 500	#ms
THRESHOLD = 10
sysstat_pid = ''
netstat_pid = ''

def die(msg):
	print msg
	skt.close()
	sys.exit()

def get_ctrl_msg(conn):
	msg = ''
	while msg!=ack :
		msg = conn.recv(1024).rstrip()

def kill_monitor():
	global monitor_group
	for monitor in monitor_group :
		while (monitor != '') :
			os.system("adb shell \"su -c 'kill -s KILL "+monitor+"'\"")
			monitor = commands.getstatusoutput("adb shell ps|grep cpulimit|awk '$2~/^"+monitor+"$/ {print $2}'")[1]
	del monitor_group[:]

def monitor_app(lmt):
	global pid
	global limit
	global pid_group
	global monitor_group
	if pid == '' : return
	if limit == lmt : return
	kill_monitor()
	if (int(lmt) < 100) :
		pid_group = commands.getstatusoutput("adb shell ps | grep "+app+" | awk '{ print $2 }'")[1].splitlines()
		for xpid in pid_group :
			trial = 0
			while (trial < MAX_TRY) :
				trial += 1
				os.system("adb shell \"su -c 'nohup /data/local/tmp/cpulimit -p "+xpid+" -l "+lmt+" >/dev/null 2>&1 &'\"")
				mpid = commands.getstatusoutput("adb shell ps | grep cpulimit | tail -n 1 | awk '{ print $2 }'")[1]
				if (mpid != '') : break
			monitor_group.append(mpid)
	limit = lmt
	print "\tpid(s): "+','.join(pid_group)+" , limit: "+limit+" , monitor(s): "+','.join(monitor_group)

def stats_start():
	global sysstat_pid
	global netstat_pid
	os.system("stdbuf -o0 adb shell top -d "+str(INTERVAL/1000)+" -s cpu | stdbuf -o0 awk '$1~/^"+pid+"$/ {print $3,$7}' > "+SYSFILE+" &")
	sysstat_pid = commands.getstatusoutput("ps|grep awk|tail -n 1|cut -d' ' -f1")[1]

def stats_end():
	os.system("kill -s KILL "+sysstat_pid)

print '\t--- SANSE Emulator ---\n'
# Create and bind socket
skt = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
skt.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
try : skt.bind((HOST, PORT))
except socket.error as msg : die('Bind failed. [' + str(msg[0]) + '] ' + msg[1])

# Identify device
strIpAddr = commands.getstatusoutput("adb shell ip -f inet addr show wlan0 2>/dev/null \
			| grep inet | awk '{print $2}' | awk -F'/' '{print $1}'  ")[1]
if (strIpAddr=='') : die('Could NOT get device IP !!')
print 'Device IP :\t' + strIpAddr

# Parse default params
dict_defaults = {}
if not os.path.isfile(NET_DEFAULTS) : die('Default params "' + NET_DEFAULTS + '" could NOT be loaded !!')
xmlDefault = minidom.parse(NET_DEFAULTS).getElementsByTagName('defaults')[0].getElementsByTagName('params')
for default in xmlDefault :
	name = default.attributes['name'].value
	params = default.getElementsByTagName('net')
	dict_defaults[name] = params

# Parse scenario file
if len(sys.argv) < 2 : die('usage: ' + sys.argv[0] + ' <scenario.xml>')
if not os.path.isfile(sys.argv[1]) : die('Scenario file "' + sys.argv[1] + '" NOT found !!')
xmlScene = minidom.parse(sys.argv[1]).getElementsByTagName('scenario')[0]
app = xmlScene.attributes['app'].value
name = xmlScene.attributes['name'].value
xmlState = xmlScene.getElementsByTagName('state')
print 'App       :\t'+app+'\nScenario  :\t'+name+'\n# States  :\t'+str(len(xmlState))+'\n---------------'

# Listen to socket
skt.listen(5)

# launch SANSE client
os.system("nohup adb shell \"su -c 'am instrument -e class com.robustnet.appcontroller.AppController -w com.google.android.apps.authenticator2.test/android.test.InstrumentationTestRunner'\" & ")

# Accept socket connection
conn, addr = skt.accept()
print 'Connected with ' + addr[0] + ':' + str(addr[1])

# netemu init
if __debug__:
	shaping1 = Shaping(rate=100000, delay_delay=0)
	UtcTester = AtcdLinuxShaper()
	UtcTester.initTask(NETEMU_WAN, NETEMU_LAN)

for state in xmlState :
	print '\n\n\t-- State --\n\tID = ' + state.attributes['ID'].value
	# UI state transition
	cmd = ''
	ack = ''
	xmlUI = state.getElementsByTagName('ui')
	if len(xmlUI) :
		cmd = xmlUI[0].attributes['cmd'].value
		ack = xmlUI[0].attributes['ack'].value
		print '\tUI = cmd: ' + cmd
		conn.send("cmd="+cmd+",ack="+ack)

	# System Emulation
	xmlSys = state.getElementsByTagName('sys')
	if len(xmlSys) :
		cpu = str(min(max(int(xmlSys[0].attributes['cpu'].value),MIN_CPU),MAX_CPU))
		print '\tSys = CPU: ' + cpu + ' %'
		monitor_app(cpu)

	# Network Emulation
	xmlNet = state.getElementsByTagName('net')
	if len(xmlNet) :
		if 'ref' in xmlNet[0].attributes.keys() :
			ref = xmlNet[0].attributes['ref'].value
			if ref!='' :
				if ref in dict_defaults.keys() : xmlNet = dict_defaults[ref]
				else : die("Parameters for default '"+ref+"' NOT found !!")
		delay = min(max(int(xmlNet[0].attributes['delay'].value),MIN_DELAY),MAX_DELAY)
		bw = min(max(int(xmlNet[0].attributes['bw'].value),MIN_BW),MAX_BW)
		drop = min(max(int(xmlNet[0].attributes['drop'].value),MIN_DROP),MAX_DROP)
		print '\tNet = Delay: ' + str(delay) + ' ms ,  BW: ' + str(bw) + ' kbps,  Drop: ' + str(drop) +' %'
		if __debug__:
			shaping1 = Shaping(rate=bw, delay_delay = delay, loss_percentage= drop)
			mark1 = 10
			UtcTester.initialize_shaping_system()
			UtcTester._shape_interface(mark1, UtcTester.lan, strIpAddr, shaping1)

	# Synchronization
	xmlSync = state.getElementsByTagName('sync')
	if len(xmlSync) :
		syncTime = xmlSync[0].attributes['time'].value
		if syncTime.isdigit() :
			print '\tSync = ' + syncTime + ' ms'
			time.sleep (int(syncTime)/1000.0)
		else:
			print '\tSync = ' + syncTime
			get_ctrl_msg(conn)
		if (cmd == "launch") :
			pid = commands.getstatusoutput("adb shell ps|grep "+app+"|tail -n 1|awk '{ print $2 }'")[1]
			if pid == '': die("Could NOT launch app '"+app+"' !!")
			else : print '\tLaunched '+app+'\t PID = '+pid
			stats_start()

stats_end()

# Evaluation
os.system("gnuplot results.gplot")

die('\nScenario ' + name + ' emulated.\n---------------')

