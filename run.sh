#!/bin/bash
BASE_DIR=`dirname $(realpath $0)`
CONFIG_DIR="${BASE_DIR}/scenes"
RESULT_DIR="${BASE_DIR}/runs"
EXE_PATH="${BASE_DIR}/emulator.py"
PLOT_PATH="${BASE_DIR}/results.gplot"
#---Primitives---
die() { echo -e "$1"; exit 1; }
#---Prologue---
if [[ $# -ne 4 ]]; then
	die "usage:\t$0 <app_name> <ui_scenario> <emulation_scenario> <# iterations>\ne.g. :\t$0 youtube play congestion 5"
fi
BENCH="${CONFIG_DIR}/$1.$2.$3.xml"
if [ ! -f ${BENCH} ]; then
	die "Error: Scenario file '${BENCH}' NOT found !"
fi
RESULT_DIR="${RESULT_DIR}/$1.$2.$3/`date`"
for i in $(seq 1 $4)
do
	RUN_DIR="${RESULT_DIR}/$i"
	mkdir -p """${RUN_DIR}"""
	pushd """${RUN_DIR}""" > /dev/null
	if [ -f ${PLOT_PATH} ]; then
		cp ${PLOT_PATH} """${RUN_DIR}/"""
	fi
	python -O ${EXE_PATH} ${BENCH} | tee run.log
	popd > /dev/null
	sleep 2m # good for experiments.
done
#---Epilogue---
echo -e "\n---Done---\nResults in ${RESULT_DIR}\n"

