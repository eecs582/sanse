from Tkinter import *
import threading
import commands
import os

MAX_TRY=100

class TimedThread(threading.Thread):
	def __init__(self, name='TimedThread'):
		self._stopevent = threading.Event()
		self._sleepperiod = 10.0
		threading.Thread.__init__(self, name=name)
	def run(self):
		while not self._stopevent.isSet():
			running_apps()
			self._stopevent.wait(self._sleepperiod)
	def join(self, timeout=None):
		self._stopevent.set()
		threading.Thread.join(self, timeout)

def running_apps():
	print "polling"
	query = commands.getstatusoutput("adb shell top -n 1 -s cpu | awk -v OFS=',' '$8~/^fg$/ && $9~/^u0_a/ {print $1,$9,$10,$3,$7}'")[1].translate(None,'\r').split('\n')
	menu = optApps.children["menu"]
	menu.delete(0, "end")
	for row in query :
		app = '.'.join(row.split(',')[2].split('.')[2:]).replace('android.','')
		dict_pid[app] = row.split(',')[0]
		dict_usr[app] = row.split(',')[1]
		dict_proc[app] = row.split(',')[2]
		dict_cpu[dict_pid[app]] = row.split(',')[3]
		dict_mem[dict_pid[app]] = row.split(',')[4]
		menu.add_command(label=app, command=lambda v=app: strApp.set(v))
	if pid != '' : strRunInfo.set("CPU: "+dict_cpu[pid]+"\t\t\tMemory: "+dict_mem[pid])
	print "..."

def select_app():
	if strApp.get() == "<Select App>" : return
	app = strApp.get()
	global pid
	pid = dict_pid[app]
	global usr
	usr = dict_usr[app]
	global proc
	proc = dict_proc[app]
	strAppInfo.set("PID= "+pid+"  User= "+usr+"  Process= "+proc)
	if dict_limit.get(pid,None) != None : sclLimit.set(dict_limit[pid])
	else : sclLimit.set(100)
	strRunInfo.set("CPU: "+dict_cpu[pid]+"\t\t\tMemory: "+dict_mem[pid])

def monitor_app(e):
	lmt = str(sclLimit.get())
	if pid == '' : return
	if lmt == '0' : return
	if dict_limit.get(pid,'100') == lmt : return
	trial = 0
	while (trial < MAX_TRY) :
		trial += 1
#		adb shell "su -c 'kill -s KILL 12345'"
		if dict_monitor.get(pid,'') != '' : os.system("adb shell \"su -c 'kill -s KILL "+dict_monitor[pid]+"'\"")
#		adb shell "su -c 'nohup /data/local/tmp/cpulimit -p 12345 -l 50 >/dev/null 2>&1 & echo $!'"
		os.system("adb shell \"su -c 'nohup /data/local/tmp/cpulimit -p "+pid+" -l "+lmt+" >/dev/null 2>&1 & echo $!'\"")
		mpid = commands.getstatusoutput("adb shell ps | grep cpulimit | tail -n 1 | awk '{ print $2 }'")[1]
		if mpid in dict_monitor.values() : mpid = ''
		if mpid != '' : break
	dict_limit[pid] = lmt
	dict_monitor[pid] = mpid
	print "pid: "+pid+" , limit: "+dict_limit[pid]+" , monitor: "+dict_monitor[pid]

def on_close():
	for pid, mpid in dict_monitor.items() :
		if mpid != '' : os.system("adb shell \"su -c 'kill -s KILL "+mpid+"'\"")
	global poller
	poller.join()
	win.destroy()

pid = ''
usr = ''
proc = ''
dict_pid = {}
dict_usr = {}
dict_proc = {}
dict_cpu = {}
dict_mem = {}
dict_limit = {}
dict_monitor = {}

win = Tk()
win.protocol("WM_DELETE_WINDOW", on_close)
#win.attributes('-zoomed', True)
win.geometry('{}x{}'.format(650,250))
win.wm_title("Android System Emulator")

strDevInfo = commands.getstatusoutput("adb shell uname -a | awk '{print $13,$3}'")[1]
lblDevice = Label(win, text=strDevInfo, height=1)
lblDevice.pack(side='top', padx=0, pady=0)

strApp = StringVar(win)
strApp.set("<Select App>")
optApps = OptionMenu(win, strApp, *[''])
optApps.pack(side='top', padx=0, pady=5)

btnSetApp = Button(win, text="Select", command=select_app)
btnSetApp.pack(side='top', padx=0, pady=0)

strRunInfo = StringVar(win)
strRunInfo.set("CPU: --\t\t\tMemory: --")
lblRunInfo = Label(win, textvariable=strRunInfo, height=1)
lblRunInfo.pack(side='bottom', padx=10, pady=10)

strAppInfo = StringVar(win)
strAppInfo.set("No app selected")
lblInfo = Label(win, textvariable=strAppInfo, height=1)
lblInfo.pack(side='bottom', padx=10, pady=0)

sclLimit = Scale(win, from_=0, to=100, length=600, resolution=5, tickinterval=10, orient=HORIZONTAL)
sclLimit.bind("<ButtonRelease-1>", monitor_app)
sclLimit.pack(side='left', padx=10, pady=0)
sclLimit.set(100)

poller = TimedThread()
poller.start()
mainloop()

