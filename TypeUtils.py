

class Shaping:

    def __init__(self, rate = 100, loss_percentage=0, loss_correlation=0, delay_delay = 100, delay_correlation = 0.001, delay_jitter = 0,
                 reorder_percentage= 0, reorder_correlation = 0,reorder_gap = 0, corruption_percentage = 0,  corruption_correlation =0):
        self.rate = rate
        self.loss_percentage = loss_percentage
        self.loss_correlation = loss_correlation
        self.delay_delay = delay_delay
        self.delay_correlation = delay_correlation
        self.delay_jitter = delay_jitter
        self.reorder_percentage = reorder_percentage
        self.reorder_correlation = reorder_correlation
        self.reorder_gap = reorder_gap
        self.corruption_percentage = corruption_percentage
        self.corruption_correlation = corruption_correlation
        self.iptables_options = None

    def setRate(self, rate):
        self.rate = rate

    def setDelay(self, delay):
        self.delay_delay = delay






